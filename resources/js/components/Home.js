import React, {useState} from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import {Counter, Info, ContextSample, ReducerCounter} from './hooks/Hooks'

import SimpleBottomNavigation from './layouts/bottom'

const Home = () => {
    const [title, setTitle] = useState()
    const [visible, setVisible] = useState(true)
    return (
        <div className="container">
            <Router>
                <Link to="/">
                    <button>Home</button>
                </Link>
                <Link to="/counter">
                    <button>Counter</button>
                </Link>
                <Link to="/Info">
                    <button>Info</button>
                </Link>
                <Link to="/ContextSample">
                    <button>ContextSample</button>
                </Link>
                <Link to="/ReducerCounter">
                    <button>ReducerCounter</button>
                </Link>
            <hr/>
                {visible && <Route exact path="/" /> }
                {visible && <Route path="/counter" component={Counter} /> }
                {visible && <Route path="/Info" component={Info} /> }
                {visible && <Route path="/ContextSample" component={ContextSample} /> }
                {visible && <Route path="/ReducerCounter" component={ReducerCounter} /> }
            </Router>
            <button className="mt-3" onClick={()=> {
                setVisible(!visible)
            }}>{visible ? '숨기기' : '보이기'}</button>
        </div>

    )
}

export default Home

if (document.getElementById('Home')) {
    ReactDOM.render(<Home />, document.getElementById('Home'))
}

