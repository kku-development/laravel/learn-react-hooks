import React, { Component } from 'react'

class TOC extends Component {
    render() {
        console.log(this.props.data)
        var list = [];
        var data = this.props.data
        data.forEach(element => {
            list.push(<li key={element.id}><a href={"./a_html/"+element.id} onClick={function(str,e){
                e.preventDefault()
                this.props.onChangePage(element.id, str)
            }.bind(this, '인자값테스트')}>{element.title}</a></li>)
        });
        return (
            <nav>
                <ul>
                    {list}
                </ul>
            </nav>
        )
    }
}

export default TOC

