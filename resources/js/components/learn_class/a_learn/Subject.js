import React, { Component } from 'react'

export default class Subject extends Component {
    render() {
        return (
            <div>
                <h1><a href="/" onClick={function(e) {
                    e.preventDefault()
                    this.props.onChangePage()
                }.bind(this)}>{this.props.title}</a></h1>
                <p>{this.props.sub}</p>
            </div>
        )
    }
}
