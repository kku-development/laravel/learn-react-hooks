import React, { Component } from 'react'

export default class CreateContent extends Component {
    render() {
        return (
            <article>
                <h2>Create</h2>
                <form action="/create_process" method="POST" onSubmit={function(e){
                    e.preventDefault()
                    alert('submit')
                }.bind(this)}>
                    <p><input type="text" name="title" id="title" placeholder="title"/></p>
                    <p><textarea name="desc" id="desc" cols="30" rows="10" placeholder="description"></textarea></p>
                    <p><input type="submit" value="create"/></p>
                </form>
            </article>
        )
    }
}
