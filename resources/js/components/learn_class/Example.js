import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import TOC      from './a_learn/TOC'
import ReadContent  from './a_learn/ReadContent'
import Subject  from './a_learn/Subject'
import Control  from './crud/Control'
import CreateContent  from './crud/CreateContent'

import '../../css/example.css'

class Example extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // mode:'read',
            mode:'create',
            selectedContentId: 1,
            str: '',
            subject:{title:'WEB', sub:'World Wide Web !'},
            welcome:{title:'Welcome', desc:'Hello, React'},
            contents:[
                {id:1, title:'HTML', desc:'HTML is for information'},
                {id:2, title:'CSS', desc:'CSS is for design'},
            ]
        }
    }
    render() {
        let _title, _desc, _article = null;
        if(this.state.mode === 'welcome') {
            _title = this.state.welcome.title
            _desc = this.state.welcome.desc
            _article = <ReadContent title={_title} desc={_desc}></ReadContent>
        } else if(this.state.mode === 'create') {
            _article = <CreateContent></CreateContent>
        } else {
            let selectedContentId = this.state.selectedContentId-1
            _title = this.state.contents[selectedContentId].title
            _desc = this.state.contents[selectedContentId].desc
            _article = <ReadContent title={_title} desc={_desc}></ReadContent>
        }
        return(
            <div className="container">
                <Subject title={this.state.subject.title} sub={this.state.subject.sub} onChangePage={function(e){
                    this.setState({
                        mode:'welcome'
                    })
                }.bind(this)}></Subject>

                <TOC data={this.state.contents} onChangePage={function(id, str){
                    console.log(str)
                    this.setState({
                        mode:'read',
                        selectedContentId: id,
                    })
                }.bind(this)}></TOC>

                <Control onChangeMode={function(type){
                    this.setState({
                        mode:type
                    })
                }.bind(this)}></Control>

                {_article}

            </div>
        )
    }
}

export default Example

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'))
}
